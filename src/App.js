import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import TitreContext from "./components/TitreContext";
import Footer from "./components/Footer"



function App() {
  return (
    <><><Router>
      <TitreContext.Provider value="À propos de moi">
        <Navbar />
      </TitreContext.Provider>
    </Router></><Footer /></>
  );
}

export default App;
