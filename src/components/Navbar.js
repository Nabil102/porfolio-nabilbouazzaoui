import React from "react";
import logo from "../logo.png";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import AboutMe from "./About";
import Header from "./Header";
import Projects from "./Projects"
import Contact from "./Contact"
import Services from "./Services";
import i18next from 'i18next'

const Navbar = () => {

  const change = (option) => {

   localStorage.setItem('lang', option.target.value)
   window.location.reload();

  }

const lang = localStorage.getItem('lang') || 'fr';
i18next.changeLanguage(lang);

  return (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-light bg-dark">
        <div className="container">
          <Link to="/">
            <img src={logo} alt="logo.." className="logo"></img>
          </Link>
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <Link to="/" className="link">
                Accueil
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/About" className="link">
                A propos
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/Projects" className="link">
                Projets
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/Contact" className="link">
                Contact
              </Link>
            </li>
          </ul>
        </div>
        <select className="custom-select custom-select-lg mb-3 mb-5 mx-3" onChange={change} value={lang}>
          <option value="fr">Français</option>
          <option value="en">Anglais</option>
        </select>
      </nav>

      <Switch>
        <Route exact path="/">
          <Header />
          <AboutMe />
          <Services />
          <Projects/>
        </Route>
        <Route exact path="/About" component={AboutMe}></Route>

        <Route exact path="/Projects">
          <div>
          <Projects/>
          </div>
        </Route>
        <Route exact path="/Contact">
          <div>
             <Contact/>
          </div>
        </Route>
      </Switch>
    </Router>
  );
};

export default Navbar;
