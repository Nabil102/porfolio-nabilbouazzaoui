
export const themes = {
    dark : {
        color : 'white',
        background : 'green',
        padding : '10px',
    },
    light : {
        color : 'black',
        background : 'white',
        padding : '10px'
    }
}; 
