/* eslint-disable react/jsx-no-comment-textnodes */
import React, { useContext } from "react";
import { useState } from "react";
import { themes } from "./Theme";
import author from "../maPhoto.jpg";
import TitreContext from "./TitreContext";
import { useTranslation } from 'react-i18next'


const AboutMe = () => {
  const { t, i18n } = useTranslation('Header');
  const [theme, setTheme] = useState(themes.dark);

  const toggleTheme = () =>
    theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);

  const context = useContext(TitreContext);

  return (
    <div className="container py-5">
      <div className="row">
        <div className="col-lg-5 col-mx-12">
          <div className="container-img" data-aos="fade-right">
            <img src={author} alt="author..." className="profile-img" />
          </div>
        </div>
        <div className="col-lg-7 col-mx-12">
          <h1 className="about-head">{context}</h1>
          <div className=" colorbckgroun p-2" data-aos="fade-left">
            <p>
              J’ai développé mon intérêt pour le développement Web au fil de temps. J’ai découvert ce métier pendant que je cherchais un changement de carrière. Au début j’ai commencé par faire des petites formation en ligne, question de savoir à quoi m'attendre. Puis après j’ai décidé de commencer une formation plus sérieuse dans le domaine. Présentement, je suis étudiant en AEC développent Web au cegep Garneau. Je suis passionné par le développement Front-End. Depuis que j’ai intégré cette formation au début d’année, j’ai acquis des compétences en intégration Front-end HTML, CSS et javascript, ReactJs, PHP, Laravel. Bootstrap. Programation Back-end avec Nodjs, Express, Mysql, MongoDB, Rest API et Postman
              Je viens d’un milieu plutôt scientifique avec une licence en chimie que j’ai eu au Maroc. Depuis que je suis au Canada je me suis orienté dans le domaine de la programmation web. Ça me passionne et ça me rend de plus en plus curieux pour apprendre et développer mes compétences en intégration, programmation et le désigne Web. Être un développeur Wbe demande beaucoup de munitie, d'autonomie et de créativités, des qualités que j'ai acquises et développées tout au long de ma carrière. Présentement je suis à la recherche de stage de fin d'étude, Je suis quelqu'un de curieux, dynamique, autonome et motivé. 
            </p>
          </div>
        </div>

      </div>
    </div>
  );
};

export default AboutMe;
