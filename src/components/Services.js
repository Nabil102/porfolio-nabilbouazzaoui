import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDesktop, faFileCode } from "@fortawesome/free-solid-svg-icons";
import { faFacebookF} from "@fortawesome/free-brands-svg-icons";

const Services = () => {

    return (
        <>  <div><h1 className="about-head text-center mt-5" >Les services</h1></div> <div className='services'>
            <div className='py-5'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-4'>
                            <div className='box'>
                                <div className="circle"><FontAwesomeIcon className="icon" icon={faDesktop} size="2x" /></div>
                                <h3> Web Design</h3>
                                <p>Chaque projet est unique et méritte un trés bon résultat</p>
                            </div>
                        </div>
                        {/*-*/}
                        <div className='col-4'>
                            <div className='box'>
                                <div className="circle"><FontAwesomeIcon className="icon" icon={faFileCode} size="2x" /></div>
                                <h3> Web Développement</h3>
                                <p>J'utilise les technologies de pointes pour chaque site Web</p>
                            </div>
                        </div>
                        {/*-*/}
                        <div className='col-4'>
                            <div className='box'>
                                <div className="circle"><FontAwesomeIcon className="icon" icon={faFacebookF} size="2x" /></div>
                                <h3> Analyse et conception</h3>
                                <p>La réflexion et la conception la plus adapté à votre réalité</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div></>
    )
};
export default Services