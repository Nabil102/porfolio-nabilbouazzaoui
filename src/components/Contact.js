import React from "react";

function Contact() {
  return (
    <div className="container">
      <div className="contenue-form">
        <h1 className="titre-prsentation about-head "> Contactez moi</h1>
        <form method="get" action="nousContacter.html">
          <input
            type="text"
            name="nom"
            id="nom"
            placeholder="NOM"
            required
            className="nom"
          />
          <input
            type="text"
            name="prenom"
            id="prenom"
            placeholder="PRÉNOM"
            required
            className="prenom"
          ></input>
          <div>
            <input
              type="text"
              name="courriel"
              id="courriel"
              placeholder="COURRIEL"
              required
              className="tele-courriel"
            />
          </div>
          <div>
            <input
              type="tel"
              name="phone"
              placeholder="TÉLÉPHONE (000-000-0000)"
              pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
              id="tel"
              className="tele-courriel"
            />
          </div>
          <button type="button" className="btn btn-success m-5">Envoyer</button>
        </form>
      </div>
    </div>
  );
}

export default Contact;
