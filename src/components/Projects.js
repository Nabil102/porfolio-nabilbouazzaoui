/* eslint-disable react/style-prop-object */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/jsx-no-comment-textnodes */
import React from "react";
import projet1 from "../images/projet1.jpg";
import projet2 from "../images/projet2.jpg";
import projet3 from "../images/projet3.jpg";
import projet4 from "../images/projet4.jpg";

function Project() {

  return (
    <>
      <h1 className="about-head text-center m-5"> Les Projets </h1>
      <div className=" card-group container" >
        <div className="card m-4" data-aos="fade-right"
          data-aos-offset="300"
          data-aos-easing="ease-in-sine">
          <figure className="c4-izmir">
            <img src={projet1} alt="image de  porojet" />
            <figcaption>
              <h3>
                <a
                  href="https://gitlab.com/Nabil102/TP-4-Travail-Integrateur"
                  target="_blank"
                  className="card "
                  rel="noreferrer"
                >
                  Projet intégrateur
                </a>
              </h3>
            </figcaption>
          </figure>
        </div>
        <div className="card m-4" data-aos="zoom-in-up">
          <figure className="c4-izmir">
            <img src={projet2} alt="image de  porojet" />
            <figcaption>
              <h3>
                <a
                  href="https://gitlab.com/Nabil102/TP_4_Liste_Boissons.git"
                  target="_blank"
                  className="card "
                  rel="noreferrer"
                >
                  Projet avec Vue.js
                </a>
              </h3>
            </figcaption>
          </figure>
        </div>
        <div className="card m-4"  data-aos="zoom-in">
          <figure className="c4-izmir">
            <img src={projet3} alt="image de  porojet" />
            <figcaption>
              <h3>
                <a
                  href="https://gitlab.com/H.Bherer/faitmonmenage-siteweb.git"
                  target="_blank"
                  className="card "
                  rel="noreferrer"
                >
                  Projet de concéption
                </a>
              </h3>
            </figcaption>
          </figure>
        </div>
        <div className="card m-4" data-aos="zoom-in-left">
          <figure className="c4-izmir">
            <img src={projet4} alt="image de  porojet" />
            <figcaption>
              <h3>
                <a
                  href="https://compassionate-knuth-b8947b.netlify.app/"
                  target="_blank"
                  className="card "
                  rel="noreferrer"
                >
                  Projet en ReactJs et API
                </a>
              </h3>
            </figcaption>
          </figure>
        </div>
      </div>
    </>
  );
}
export default Project;
