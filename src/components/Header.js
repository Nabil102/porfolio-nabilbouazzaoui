import React from "react";
import Typed from "react-typed";
import Particles from 'react-particles-js';


const header = () => {
  return (
    <><div className="header-container">
      <Particles
      className="particles-canvas"
      params={{
        particles: {
          number: {
            value: 30,
            density: {
              enable: true,
              value_area: 900
            }
          },
          shape: {
            type: "circle",
            stroke: {
              width: 6,
              color: "#f9ab00"
            }
          }
        }
      }} />
        <div className="main-header">
          <h1>Développement Et Intégration Web</h1>
          <Typed
            className="typed-text"
            strings={[
              "Développement Web Front-End",
              "CSS",
              "HTML",
              "Javascript",
              "PHP",
              "Bootstrap",
              "Laravel",
              "React",
              "Développement Web Back-End",
              "NodeJs",
              "Express",
              "Mysql",
              "PhpMyadmin",
              "MongoDB"
            ]}
            typeSpeed={90}
            backSpeed={60}
            loop />
          <a href="/Contact" className="btn-header">Contactez-moi</a>
        </div>
      </div></>
  );
};

export default header;
